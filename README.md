# Start.spring.io <img src="https://badges.gitter.im/spring-io/initializr.svg?sanitize=true">
This repository configures a {library}[Spring Initializr] instance with a custom UI
running at https://start.spring.io. The following modules are available:

* `start-client`: client-side assets
* `start-site`: server infrastructure and metadata configuration
* `start-site-verification`: tests to verify the validity of the metadata

## Building from Source

You need Java 1.8 and a bash-like shell.

## Building

Invoke the build at the root of the project

```
    $ ./mvnw clean install
```

The project also includes a number of longer-running verification tests. They
can be built and run using the `verification` profile:

```
    $ ./mvnw -Pverification clean install
```

The project's other tests are not included in the `verification` profile. All of
the project's tests can be run using the `full` profile:

```
    $ ./mvnw -Pfull clean install
```

If building `start-client` fails, you may have an outdated cache that can be deleted as
follows:
```
    $ cd start-client
    $ rm -rf .cache node_modules
```



## run-app
Running the app locally
As long as you've built the project beforehand (in particular `start-client`), you can
easily start the app as any other Spring Boot app:

```
    $ cd start-site
    $ ../mvnw spring-boot:run
```

## How to modify the config
- 1. start-site/src/main/resources/application.yml
    - In this file, you can set the dependencies、project type、javaVersion。
    - Write your library like the following example
        ```
        dependencies:
            - name: [category name]
              content:
              - name: [library name]
              - id: [library identified id]
              - description: [library description]
              - groupID: [library groupID]
              - artifactId: [library artifactId]
              - version: [library version]
        ```
        ```
       dependencies:
        - name: LineBot
          content:
            - name: AWS S3 JDK
              id: 1
              description: aws_s3_jdk
              groupId: com.amazonaws
              artifactId: aws-java-sdk-s3
              version: 1.11.734 
        ```
        
    - For more example please see 
        - the yml file.
        - https://github.com/spring-io/start.spring.io
    
- 2. start-site/src/main/resources/settings.json
    - Change value of "projectReleases" to lock the version.The following example sets the spring version only available at [2.2.5.RELEASE].
        ```
        "projectReleases": [
        {
            "releaseStatus": "PRERELEASE",
            "refDocUrl": "https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/reference/html/",
            "apiDocUrl": "https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/api/",
            "groupId": "org.springframework.boot",
            "artifactId": "spring-boot",
            "repository": {
                "id": "spring-milestones",
                "name": "Spring Milestones",
                "url": "https://repo.spring.io/libs-milestone",
                "snapshotsEnabled": false
            },
            "version": "2.2.5.RELEASE",
            "snapshot": false,
            "preRelease": true,
            "versionDisplayName": "2.2.5.RELEASE",
            "generalAvailability": false,
            "current": true
        }
        ]
        ```
    - For more reference, please see
        - https://docs.spring.io/spring-boot/docs/
        - https://spring.io/project_metadata/spring-boot

## How to use. Supposed run it on localhost:8080

#### - Using Website
 - Do the same thing like https://start.spring.io/

#### - Using IDE
 - 1. Choose New project
 - 2. Select Spring boot project
 - 3. Set Service url = https://localhost:8080
 - 4. Choose the library you want to use.
 
== License
The start.spring.io website is Open Source software released under the
https://www.apache.org/licenses/LICENSE-2.0.html[Apache 2.0 license].
